import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../interfaces/uset.interface';
import { StorageService } from '../../services/storage.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  user: any;
  nameFlag: boolean = false;
  lastNameFlag: boolean = false;
  imageFlag: boolean = false;
  newUser!: FormGroup;
  objects = [
    {
      price: 123,
      discount: 23,
      end_date: "2020-10-29"
    },
    {
      price: 1000,
      discount: 200,
      end_date: "2021-05-27"
    },
    {
      price: 1500,
      discount: 600,
      end_date: "2021-10-29"
    }
  ]; 
  constructor(private userService: UserService, private storageService: StorageService) { }
  ngOnInit(): void {
    this.user = this.storageService.getUserFromLocalStorage() != null ? JSON.parse(this.storageService.getUserFromLocalStorage() || '{}') : this.userService.getUser();
    this.newUser = new FormGroup({
      first_name: new FormControl(this.user.first_name, Validators.required),
      last_name: new FormControl(this.user.last_name, Validators.required),
      image: new FormControl(this.user.image),
    });
  }
  setUser(user: any) {
    alert("User has been saved !");
    this.userService.setUser(user);
  }
  onSubmit(form: FormGroup) {
    console.log(form.value.last_name)
    form.value.last_name != '' || form.value.first_name != '' ? this.setUser(form.value) : alert("Muset enter first or last name !");
  }
}
