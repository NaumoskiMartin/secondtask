import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../services/storage.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user: any;
  message:string = '';
  constructor( private storageService: StorageService,private userService: UserService) { }

  ngOnInit(): void {
    this.user = this.storageService.getUserFromLocalStorage() != null ? JSON.parse(this.storageService.getUserFromLocalStorage() || '{}') : this.userService.getUser();
  }


}
