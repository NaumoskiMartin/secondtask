import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "customPipe"
})
export class customPipe implements PipeTransform {
    transform(value: any, price: any, discount: any, end_date: any) {
        let today = new Date();
        let calculate = 0;
        function calculateNumber() {
            if (value == 'Sale_price') {
                calculate = price - discount;;
            }
            else if (value == "Weekly_payment") {
                calculate = (price - discount) / 4;
            }
            else if (value == "Weekly_discount") {
                calculate = discount / 4;
            }
        } 
        if (today.getUTCFullYear() < end_date.split("-")[0]) {
            calculateNumber();
        }
        else if (today.getUTCFullYear() == end_date.split("-")[0] && today.getUTCMonth() + 1 < end_date.split("-")[1]) {
            calculateNumber();
        }
        else if (today.getUTCMonth() + 1 == end_date.split("-")[1] && today.getUTCDate() <= end_date.split("-")[2]) {
            calculateNumber();
        }
        else {
            if (value == 'Sale_price' || value == "Weekly_payment") {
                calculate = price;
            }
        }
        return calculate;
    }
}