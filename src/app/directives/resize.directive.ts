import { Directive, ElementRef, HostListener, OnInit } from '@angular/core';

@Directive({
  selector: '[appResize]'
})
export class ResizeDirective implements OnInit {
  constructor(private elementRef: ElementRef) { }
  nativeElement = this.elementRef.nativeElement;
  ngOnInit() {

  }
  @HostListener('mouseover') makeSmaller() {
    this.elementRef.nativeElement.style.height = "50px";
  }
  @HostListener('mouseout') makeBigger() {
    this.elementRef.nativeElement.style.height = "20px";
  }
}
