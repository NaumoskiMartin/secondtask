import { Injectable } from '@angular/core';
import { User } from '../interfaces/uset.interface';
import { StorageService } from './storage.service'
@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: User = { first_name: 'test', last_name: 'tester', image: 'https://img.favpng.com/10/14/2/avatar-computer-icons-user-profile-business-png-favpng-uVq05zLDgFqFDHD2EF87qrVMA.jpg' }

  constructor(private storageService: StorageService) { }

  getUser(): User {
    return this.user;
  }
  setUser(user:User) {
    this.user = user;
    this.storageService.setUserLocalStorage(this.user)
  }
}
