import { Injectable } from '@angular/core';
import { User } from '../interfaces/uset.interface';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }
  setUserLocalStorage(user:User){
    localStorage.setItem('user',JSON.stringify(user));
  }
  getUserFromLocalStorage(){
    let user = localStorage.getItem('user') != null ? localStorage.getItem('user') : null;
    return user;
  }
}
