export interface User {
    first_name: string;
    last_name: string;
    image: 'https://img.favpng.com/10/14/2/avatar-computer-icons-user-profile-business-png-favpng-uVq05zLDgFqFDHD2EF87qrVMA.jpg';
}